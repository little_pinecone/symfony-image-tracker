Image tracker
========================

The application is a sample project build on Symfony 4.0.

Requirements
------------

  * PHP 7.1.3 or higher;
  * cURL PHP extension enabled;
  * composer;
  * npm;
  * and the [usual Symfony application requirements][1].

Usage
-----

Install dependencies:

```bash
$ composer install
$ npm install
```

Create database and run migrations:

```bash
$ bin/console doctrine:migrations:migrate
```

Compile assets:

```bash
$ ./node_modules/.bin/encore dev
```

Run the built-in web server and access the application in your
browser:

```bash
$ php bin/console server:run
```

Alternatively, you can [configure a fully-featured web server][2] like Nginx
or Apache to run the application.

Tracking images
---------------

* From the application menu choose "Track image change", fill up the form, and select an image you want to check.
* All images that were changed between checks are listed in the Dashboard.
* You can also use the command:

```bash
$ bin/console image:track-changes /images/games/104.jpg --page_url=https://www.sedoc.pl/
$ bin/console image:track-changes /path/to/local/file.jpg
```

> For genearting hashes the [Perceptual image hashing for PHP](https://github.com/jenssegers/imagehash) library was used. Applied abstraction layer allows to change that dependency without modifications in the rest of the application code. 

Testing
-------

Run phpunit to see tests results.

[1]: https://symfony.com/doc/current/reference/requirements.html
[2]: https://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html