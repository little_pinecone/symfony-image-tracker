<?php

namespace App\Comparator;

use App\Exception\InvalidImageException;
use PHPUnit\Framework\TestCase;

class ImageComparatorTest extends TestCase
{

    public function testGenerateHash()
    {
        $this->expectException(InvalidImageException::class);
        $imageComparator = new ImageComparator();
        $imageComparator->generateHash('not_existing_file_path');

    }

    /**
     * @dataProvider hashesProvider
     */
    public function testImagesAreDifferent(string $currentHash, string $savedHash, bool $expected)
    {
        $imageComparator = new ImageComparator();
        $this->assertEquals($expected, $imageComparator->imagesAreDifferent($currentHash, $savedHash));
    }

    public function hashesProvider()
    {
        return [
            'are_equal' => ['e0351f0f0f1030e', 'e0351f0f0f1030e', false],
            'are_different' => ['e0351f0f0f1030e', '20e0e55d4eaa252', true]
        ];
    }
}
