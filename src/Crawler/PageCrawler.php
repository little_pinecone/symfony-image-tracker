<?php


namespace App\Crawler;


class PageCrawler
{
    public function getPageContent(string $pageUrl)
    {
        $ch = curl_init();
        $timeout =10;
        curl_setopt($ch, CURLOPT_URL, $pageUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}