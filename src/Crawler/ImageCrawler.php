<?php


namespace App\Crawler;


use Symfony\Component\DomCrawler\Crawler;

class ImageCrawler
{
    private $pageCrawler;

    public function __construct(PageCrawler $pageCrawler)
    {
        $this->pageCrawler = $pageCrawler;
    }

    public function getImages(string $pageUrl)
    {
        $domCrawler = new Crawler($this->pageCrawler->getPageContent($pageUrl));
        $result = $domCrawler
            ->filter('img')
            ->extract(array('src'));
        return $result;
    }

}