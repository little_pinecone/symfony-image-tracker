<?php


namespace App\Controller;


use App\Analyser\ImageAnalyser;
use App\Crawler\ImageCrawler;
use App\Exception\InvalidImageException;
use App\Exception\UnwritableImageException;
use App\Form\ImageSearchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ImageController extends Controller
{
    /**
     * @Route("image/crawl", name="crawl_url_images")
     */
    public function indexAction(Request $request, ImageCrawler $imageCrawler)
    {
        $form = $this->createForm(ImageSearchType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->render('image/track_change.html.twig',[
                'images' => $imageCrawler->getImages($form->get('page_url')->getData()),
                'form'=>$form->createView()
            ]);
        }
        return $this->render('image/track_change.html.twig',[
            'images' => [],
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("image/track_change/{image_source}", name="track_image_change", requirements={"image_source"=".+"})
     */
    public function trackChangeAction(ImageAnalyser $imageAnalyser, string $image_source)
    {
        try{
            $imageAnalyser->analyseImage($image_source);
        } catch (InvalidImageException $e) {
            dump($e->getMessage());
            die();
        } catch (UnwritableImageException $e) {
            dump($e->getMessage());
            die();
        }
        return $this->redirectToRoute('dashboard');
    }
}