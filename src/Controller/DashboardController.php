<?php


namespace App\Controller;


use App\Entity\Image;
use App\Repository\ImageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends Controller
{
    /**
     * @Route("/", name="dashboard")
     */
    public function indexAction()
    {
        /** @var ImageRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Image::class);
        return $this->render('dashboard/index.html.twig',[
            'images' => $repo->findChangedImages(),
        ]);
    }
}