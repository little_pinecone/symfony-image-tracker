<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $filePath;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $changeTrackedAt;

    /**
     * @ORM\Column(type="string")
     */
    private $latestHash;

    public function getId()
    {
        return $this->id;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
        return $this;
    }

    public function getLatestHash()
    {
        return $this->latestHash;
    }

    public function setLatestHash(string $latestHash)
    {
        $this->latestHash = $latestHash;
        return $this;
    }

    public function getChangeTrackedAt()
    {
        return $this->changeTrackedAt;
    }

    public function setChangeTrackedAt(\DateTime $changeTrackedAt)
    {
        $this->changeTrackedAt = $changeTrackedAt;
        return $this;
    }

}