<?php


namespace App\Analyser;


use App\Comparator\ImageComparator;
use App\Entity\Image;
use App\Exception\InvalidImageException;
use App\Exception\UnwritableImageException;
use App\Repository\ImageRepository;

class ImageAnalyser
{
    private $imageComparator;
    private $imageRepository;

    public function __construct(ImageComparator $imageComparator, ImageRepository $imageRepository)
    {
        $this->imageComparator = $imageComparator;
        $this->imageRepository = $imageRepository;
    }

    /**
     * @param string $imageSource
     * @return Image
     * @throws InvalidImageException
     * @throws UnwritableImageException
     */
    public function analyseImage(string $imageSource): Image
    {
        $currentHash = $this->imageComparator->generateHash($imageSource);
        $savedImage = $this->imageRepository->findOneBy(['filePath' => $imageSource]);
        if (is_null($savedImage)) {
            $savedImage = $this->createNewImage($imageSource, $currentHash);
        }
        if ($this->imageComparator->imagesAreDifferent($currentHash, $savedImage->getLatestHash())) {
            $savedImage = $this->updateImage($savedImage, $currentHash);
        }
        $this->imageRepository->save($savedImage);

        return $savedImage;
    }

    private function createNewImage(string $imageSource, string $currentHash): Image
    {
        /** @var Image $image */
        $savedImage = new Image();
        $savedImage
            ->setFilePath($imageSource)
            ->setLatestHash($currentHash);
        return $savedImage;
    }

    private function updateImage(Image $savedImage, string $currentHash): Image
    {
        $savedImage
            ->setLatestHash($currentHash)
            ->setChangeTrackedAt(new \DateTime("now"));
        return $savedImage;
    }
}