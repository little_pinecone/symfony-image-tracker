<?php

namespace App\Command;


use App\Analyser\ImageAnalyser;
use App\Exception\InvalidImageException;
use App\Exception\UnwritableImageException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TrackImageCommand extends ContainerAwareCommand
{
    private $imageAnalyser;

    public function __construct(ImageAnalyser $imageAnalyser, string $name = null)
    {
        parent::__construct($name);
        $this->imageAnalyser = $imageAnalyser;
    }

    protected function configure()
    {
        $this
            ->setName('image:track-changes')
            ->setHelp('This command allows you to acknowledge whether a tracked image was changed on the given website.')
            ->addArgument('image_path', InputArgument::REQUIRED, 'The path of the image file.')
            ->addOption('page_url', null, InputOption::VALUE_OPTIONAL, 'The url of the site where the image is used.', '');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try{
            $imageSource = $input->getOption('page_url') . $input->getArgument('image_path');
            $this->imageAnalyser->analyseImage($imageSource);
        } catch (InvalidImageException $e) {
            $output->writeln('<error>Image processing error has occurred.</error>');
        } catch (UnwritableImageException $e) {
            $output->writeln('<error>Image saving error has occurred.</error>');
        }
    }
}