<?php


namespace App\Repository;


use App\Entity\Image;
use App\Exception\UnwritableImageException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Image::class);
    }

    public function findChangedImages()
    {
        $qb = $this->createQueryBuilder('i');
        return $qb->select()
            ->where($qb->expr()->isNotNull('i.changeTrackedAt'))
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Image $image
     * @throws UnwritableImageException
     */
    public function save(Image $image)
    {
        try{
            $this->_em->persist($image);
            $this->_em->flush();
        } catch (\Exception $e) {
            throw new UnwritableImageException('Image could not be saved.');
        }
    }
}