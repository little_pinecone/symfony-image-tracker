<?php


namespace App\Comparator;


use App\Exception\InvalidImageException;
use Jenssegers\ImageHash\ImageHash;

class ImageComparator
{
    /**
     * @param string $imageSource
     * @return int
     * @throws InvalidImageException
     */
    public function generateHash(string $imageSource)
    {
        $hasher = new ImageHash();
        try{
            return $hasher->hash($imageSource);
        } catch (\InvalidArgumentException $e) {
            throw new InvalidImageException("Unable to get image: $imageSource");
        }
    }

    public function imagesAreDifferent(string $currentHash, string $savedHash)
    {
        if($currentHash !== $savedHash){
            return true;
        } else {
            return false;
        }
    }

}